package com.epam.rd.java.basic.task8.entity.growing;

import java.util.ArrayList;
import java.util.List;

public class GrowingTips {
    private List<Tempreture> tempreture;
    private List<Lighting> lighting;
    private List<Watering> watering;

    public List<Tempreture> getTemperature() {
        if (tempreture == null){
            tempreture = new ArrayList<>();
        }
        return tempreture;
    }

    public List<Lighting> getLighting() {
        if (lighting == null){
            lighting = new ArrayList<>();
        }
        return lighting;
    }

    public List<Watering> getWatering() {
        if (watering == null){
            watering = new ArrayList<>();
        }
        return watering;
    }

    @Override
    public String toString() {
        return "{" +
                "tempreture=" + tempreture +
                ", lighting=" + lighting +
                ", watering=" + watering +
                '}';
    }
}
