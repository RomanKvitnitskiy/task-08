package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flowerList;

    public List<Flower> getFlower() {
        if (flowerList == null){
            flowerList = new ArrayList<>();
        }
        return flowerList;
    }

    @Override
    public String toString() {
        if (flowerList == null || flowerList.isEmpty()){
            return "There are no flowers";
        }
        StringBuilder str = new StringBuilder();
        for (Flower flower : flowerList){
            str.append(flower).append("\n");
        }
        return str.toString();
    }
}
