package com.epam.rd.java.basic.task8.constants;

public enum XML {
    FLOWERS("flowers"),
    FLOWER("flower"),
    FLOWER_NAME("name"),
    FLOWER_SOIL("soil"),
    FLOWER_ORIGIN("origin"),
    FLOWER_MULTIPLYING("multiplying"),
    VISUAL_PARAMETERS("visualParameters"),
    STEM_COLOUR("stemColour"),
    LEAF_COLOUR("leafColour"),
    AVE_LEN_FLOWER("aveLenFlower"),
    GROWING_TIPS("growingTips"),
    TEMPRETURE("tempreture"),
    LIGHTING("lighting"),
    LIGHT_REQUIRING("lightRequiring"),
    WATERING("watering"),
    MEASURE("measure");

    private final String value;

    XML(String value) {
        this.value = value;
    }

    public boolean equalsTo(String name) {
        return value.equals(name);
    }

    public String value() {
        return value;
    }
}
