package com.epam.rd.java.basic.task8.entity.parameters;

import java.util.ArrayList;
import java.util.List;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private List<AveLenFlower> aveLenFlower;

    public String getStemColour() {
        return stemColour;
    }
    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }
    public String getLeafColour() {
        return leafColour;
    }
    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }
    public List<AveLenFlower> getAveLenFlower() {
        if (aveLenFlower == null){
            aveLenFlower = new ArrayList<>();
        }
        return aveLenFlower;
    }

    @Override
    public String toString() {
        return "{" +
                "stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower=" + aveLenFlower +
                '}';
    }
}
